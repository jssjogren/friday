# Friday
Friday is a voice-activated Discord Bot for our personal Discord server. It utilizes the DiscordJS/OPUS community library/api.

---

## About Friday
Currently commands are being captured via WitAI speech-to-text and are being parsed in the services/speech_recognition.js file.

I have created a few different classes to aid in separation of responsibilities for the commands. So, for instance, Phasmophobia evidence tracking
has received its own class. Whereas asking Friday what time it is will be delegated to the General class.

## Voice/Text Commands
The activating voice command is **"friday"** with some variations taken into account based on WitAI's understanding.

**Voice**
 * **new game** - starts a new round of Phasmophobia.
 * **add evidence** - can be followed by one of the following types of evidence in phasmophobia.
	* ghost orbs
	* fingerprints
	* freezing temperatures
	* spirit box
	* readings *(note: could not use 'EMF Readings' as WitAI wouldn't understand the word EMF)*
	* ghost writing
 * **remove last evidence** - removes the last piece of evidence added.
 * **identify ghost** - based on the 3 pieces of evidence currently added, alerts you as to the type of ghost you're facing.
 * **explain [ghost name]** - ask friday to explain the unique traits of a specific ghost to you.
 * **solitary confinement** - sends Abe to solitary confinement channel.
 * **what time is it** - tells you the current time in EST.
 * **thanks friday** - give some thanks to Friday after a job well done.

**Text**
 * **join** - has Friday join your current voice channel.
 * **leave** - has Friday leave the voice channel she is in.

## List of [Phasmophobia ghosts](https://phasmophobia.fandom.com/wiki/Ghosts) accepted by Friday
 * Shade
 * Phantom
 * Jinn
 * Yurei
 * Mare
 * Demon
 * Banshee
 * Revenant
 * Oni
 * Poltergeist
 * Spirit
 * Wraith
 * Yokai
 * Hantu
