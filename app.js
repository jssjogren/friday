const config = require('./config')
const Discord = require('discord.js');
const client = new Discord.Client({ intents: ['GUILDS', 'GUILD_MESSAGES', 'GUILD_VOICE_STATES']});
const SpeechRecognition = require('./services/speech_recognition')

// change CVE and SpeechRecognition class
// they shouldn't need to have the discord client as a constructor value
// I admit this part was quickly hacked together
// const cve = new CVE(client)
const speechRecognition = new SpeechRecognition(client)

// // polls for new CVE's every 15 minutes
// cve.poll(cve.latest, 15)

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);

  client.user.setPresence({
    game: {
      name: 'over you',
      type: "Watching",
      url: "https://discordapp.com/"
    }
  });
})

client.on('message', msg => {

  // temp hard code command
  if (msg.content === 'friday join') {
    speechRecognition.startListen(msg)
  }

  if (msg.content === 'friday leave') {
    console.log('Should leave all voice channels')
    // console.log(client.voiceConnections)
    client.voiceConnections.map((chan) => {
      chan.disconnect()
    })
    speechRecognition.stopListen(msg)
  }

  if(msg.content === 'jeb terminate') {
    console.log('Terminating client...')
    client.destroy()
    process.exit()
  }

  let parts = msg.content.split(' ')
  let cmd = parts[0]
  let options = parts.slice(1, parts.length).join(' ')


  // if (methodManager.isMethod(cmd)) {
  //   let props = {client, msg, parts, options, cve}
  //   methodManager.execute(cmd, props)
  // }

})

client.login(config.discord.auth_key)