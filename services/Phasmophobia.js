var evidenceLocker = [];
var ghostsEvidence = {};
var ghostsUniqueTraits = {};
const discordTTS = require('discord-tts');

module.exports = class Phasmophobia {

    constructor ()
    {
        this.initializeGhostsEvidenceList()
        this.initializeGhostsUniqueTraits()
    }

    initializeGhostsEvidenceList()
    {
        ghostsEvidence["shade"] = ['emf readings', 'ghost orbs', 'ghost writing'];
        ghostsEvidence["phantom"] = ['emf readings', 'ghost orbs', 'freezing temperatures'];
        ghostsEvidence["jinn"] = ['emf readings', 'ghost orbs', 'spirit box'];
        ghostsEvidence["yurei"] = ['ghost orbs', 'ghost writing', 'freezing temperatures'];
        ghostsEvidence["mare"] = ['ghost orbs', 'freezing temperatures', 'spirit box'];
        ghostsEvidence["demon"] = ['ghost writing', 'freezing temperatures', 'spirit box'];
        ghostsEvidence["banshee"] = ['emf readings', 'freezing temperatures', 'fingerprints'];
        ghostsEvidence["revenant"] = ['emf readings', 'ghost writing', 'fingerprints'];
        ghostsEvidence["oni"] = ['emf readings', 'ghost writing', 'spirit box'];
        ghostsEvidence["poltergeist"] = ['ghost orbs', 'spirit box', 'fingerprints'];
        ghostsEvidence["spirit"] = ['ghost writing', 'spirit box', 'fingerprints'];
        ghostsEvidence["wraith"] = ['freezing temperatures', 'spirit box', 'fingerprints'];
        ghostsEvidence["yokai"] = ['ghost orbs', 'ghost writing', 'spirit box'];
        ghostsEvidence["hantu"] = ['ghost orbs', 'ghost writing', 'fingerprints'];
    }

    // https://phasmophobia.fandom.com/wiki/Ghosts#Identifying_Traits_of_Ghosts
    initializeGhostsUniqueTraits()
    {
        ghostsUniqueTraits["shade"] = "Shades rarely perform actions, including hunts, in the presence of 2+ people."
        ghostsUniqueTraits["phantom"] = "Phantoms flash every 1-2 seconds during a hunt. They also disappear when a photo is taken."
        ghostsUniqueTraits["jinn"] = "Jinns interact with electronics more frequently."
        ghostsUniqueTraits["yurei"] = "Yurei drain sanity faster than any other ghosts when manifested or hunting."
        ghostsUniqueTraits["mare"] = "Mares tend to turn lights off more often, but rarely turns them back on."
        ghostsUniqueTraits["demon"] = "Demons tend hunt more frequently than other ghosts."
        ghostsUniqueTraits["banshee"] = "Banshees tend to hunt very early in the game when players have high sanity."
        ghostsUniqueTraits["revenant"] = "Revenants move faster during a hunt if a player is in its line of sight."
        ghostsUniqueTraits["oni"] = "Oni are more active when people are nearby. They're more likely to move objects at high speeds."
        ghostsUniqueTraits["poltergeist"] = "Poltergeists tend to interact with objects more frequently. They can also move multiple objects."
        ghostsUniqueTraits["spirit"] = "Spirits have no defining characteristics. My analytics have determined you're more than likely fucked."
        ghostsUniqueTraits["wraith"] = "Wraiths are supposed to rarely make footstep walking sounds. They do not leave footprints in salt."
        ghostsUniqueTraits["yokai"] = "Yokais are sensitive to human voices, allowing it to initiate hunts even at high sanity levels. However, it has reduced hearing range."
        ghostsUniqueTraits["hantu"] = "Hantus move slightly faster when hunting in cold rooms. They move at regular speed in warm rooms."
    }

    addEvidence(evidence, client, listenConnection) {
        const broadcast = client.voice.createBroadcast();

        if (evidence[2].includes("fingerprint")){
            evidenceLocker.push("fingerprints")
            console.log("Fingerprints added to evidence locker")
            broadcast.play(discordTTS.getVoiceStream("Fingerprints added to evidence locker."));

        }
        else if (evidence[2] == "ghost"&& evidence[3] == "orbs" || evidence[3] == "boards")
        {
            evidenceLocker.push("ghost orbs")
            console.log("Ghost Orbs added to evidence locker")
            broadcast.play(discordTTS.getVoiceStream("Ghost orbs added to evidence locker."));
        }
        else if (evidence[2] == "freezing" && evidence[3] == "temperatures")
        {
            evidenceLocker.push("freezing temperatures")
            console.log("Freezing Temperatures added to evidence locker")
            broadcast.play(discordTTS.getVoiceStream("Freezing temperatures added to evidence locker."));
        }
        else if (evidence[2] == "spirit" && evidence[3] == "box")
        {
            evidenceLocker.push("spirit box")
            console.log("Spirit Box added to evidence locker")
            broadcast.play(discordTTS.getVoiceStream("Spirit Box added to evidence locker."));
        }
        else if (evidence[2] == "ghostwriters" || evidence[2] == "ghostwriting" || evidence[2] == "ghost" && evidence[3] == "writing" || evidence[3] == "ridings")
        {
            evidenceLocker.push("ghost writing")
            console.log("Ghost writing added to evidence locker")
            broadcast.play(discordTTS.getVoiceStream("Ghost writing added to evidence locker."));
        }
        else if (evidence[2] == "readings" || evidence[2] == "ridings")
        {
            evidenceLocker.push("emf readings")
            console.log("EMF Readings added to evidence locker")
            broadcast.play(discordTTS.getVoiceStream("EMF Readings added to evidence locker."));
        }
        const dispatcher = listenConnection.play(broadcast);
    }

    newGame(client, listenConnection)
    {
        evidenceLocker = [];
        const broadcast = client.voice.createBroadcast()
        broadcast.play(discordTTS.getVoiceStream("New game of phasmophobia starting now."));
        const dispatcher = listenConnection.play(broadcast);
    }

    removeLastEvidence(client, listenConnection)
    {
        var removedEvidence = evidenceLocker.pop();
        const broadcast = client.voice.createBroadcast()
        broadcast.play(discordTTS.getVoiceStream(removedEvidence + " has been removed from the evidence locker."));
        const dispatcher = listenConnection.play(broadcast);
    }

    listEvidence(client, listenConnection)
    {
        var phrase = "You have collected the following evidence: "
        if (evidenceLocker.length != 0)
        {
            for (var i = 0; i < evidenceLocker.length; i++)
            {
                if (i == evidenceLocker.length - 1 && evidenceLocker.length >= 2)
                    phrase += " and " + evidenceLocker[i];
                else
                    phrase += " " + evidenceLocker[i];
            }
        }
        else
        {
            phrase = "You have collected no evidence yet.";
        }
        const broadcast = client.voice.createBroadcast()
        broadcast.play(discordTTS.getVoiceStream(phrase));
        const dispatcher = listenConnection.play(broadcast);
    }

    listTraits(client, listenConnection, ghostType)
    {
        var trait;
        if (ghostType == "phantom" || ghostType == "phantoms")
            trait = ghostsUniqueTraits["phantom"];
        else if (ghostType == "shade" || ghostType == "shades")
            trait = ghostsUniqueTraits["shade"];
        else if (ghostType == "jinn" || ghostType == "gin" || ghostType == "jinns")
            trait = ghostsUniqueTraits["jinn"];
        else if (ghostType == "your" || ghostType == "yurei" || ghostType == "yours")
            trait = ghostsUniqueTraits["yurei"];
        else if (ghostType == "mare" || ghostType == "mair" || ghostType == "mairs" || ghostType == "mares")
            trait = ghostsUniqueTraits["mare"];
        else if (ghostType == "demon" || ghostType == "demons")
            trait = ghostsUniqueTraits["demon"];
        else if (ghostType == "banshee" || ghostType == "banshees")
            trait = ghostsUniqueTraits["banshee"];
        else if (ghostType == "revenant" || ghostType == "revenants")
            trait = ghostsUniqueTraits["revenant"];
        else if (ghostType == "onis" || ghostType == "oni")
            trait = ghostsUniqueTraits["oni"];
        else if (ghostType == "poltergeist" || ghostType == "poltergeists")
            trait = ghostsUniqueTraits["poltergeist"];
        else if (ghostType == "spirit" || ghostType == "spirits")
            trait = ghostsUniqueTraits["spirit"];
        else if (ghostType == "wraith" || ghostType == "wraiths" || ghostType == "race" || ghostType == "rath")
            trait = ghostsUniqueTraits["wraith"];
        else if (ghostType == "yokai" || ghostType == "yokais" || ghostType == "yo-yo" || ghostType == "yookie" || ghostType == "yoko's" || ghostType == "yogi's")
            trait = ghostsUniqueTraits["yokai"];
        else if (ghostType == "hantu" || ghostType == "hantus" || ghostType == "haunted" || ghostType == "hunters")
            trait = ghostsUniqueTraits["hantu"];

        const broadcast = client.voice.createBroadcast();
        broadcast.play(discordTTS.getVoiceStream(trait));
        console.log(trait);
        const dispatcher = listenConnection.play(broadcast);
    }

    whatGhost(client, listenConnection) {
        var ghost = "";
        var potentialMatches = [];

        for (const [ key, value ] of Object.entries(ghostsEvidence)) {
            var match = true;
            for (var i = 0; i < evidenceLocker.length; i++)
            {
                if (!value.includes(evidenceLocker[i]))
                    match = false;
            }

            if (match)
                potentialMatches.push(key);
        }

        var phrase = "";
        if (potentialMatches.length == 1)
            phrase = "You are facing a " + potentialMatches[0];
        else {
            phrase = "You might be facing"
            for (var i = 0; i < potentialMatches.length; i++)
            {
                if (i == potentialMatches.length - 1)
                    phrase += " or a " + potentialMatches[i];
                else
                    phrase += " a " + potentialMatches[i];
            }
        }
        const broadcast = client.voice.createBroadcast();
        broadcast.play(discordTTS.getVoiceStream(phrase));
        console.log(phrase);
        const dispatcher = listenConnection.play(broadcast);
    }
}
