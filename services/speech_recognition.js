const config = require('../config')
const WitSpeech = require('node-witai-speech');
const fs = require('fs');
const path = require('path');
const Phasmophobia = require('./Phasmophobia')
const General = require('./General')
const { Readable } = require('stream');

const SILENCE_FRAME = Buffer.from([0xF8, 0xFF, 0xFE]);

class Silence extends Readable {
  _read() {
    this.push(SILENCE_FRAME);
    this.destroy();
  }
}

phasmo = new Phasmophobia();
general = new General();

module.exports = class SpeechRecognition {

  constructor(client) {
    this.client = client;
    this.listenStreams = new Map();
  }

  startListen(msg)
  {
    this.voiceChannel = msg.member?.voice.channel;
    if (this.voiceChannel) {
      this.voiceChannel.join().then( (connection) => {
        this.client.on('guildMemberSpeaking', this.handleSpeaking.bind(this))

        this.listenConnection = connection;
        this.voiceReceiver = connection.receiver;
        msg.channel.send('Now listening in **' + this.voiceChannel.name + '**.');
        this.listenConnection.play(new Silence(), { type: 'opus'} )
      });
    }
    else
    {
      msg.reply('Join a voice channel then try again!');
    }
  }

  handleSpeaking(member, speaking) {
    if (speaking.bitfield === 1)
    {
      let stream = this.listenStreams.get(member.id)
      if (!stream) {
        const audio = this.voiceReceiver.createStream(member, {mode: 'pcm'});
        audio.pipe(fs.createWriteStream('./recordings/' + member.id + '.raw_pcm'));
        audio.path = './recordings/' + member.id + '.raw_pcm';
        this.listenStreams.set(member.id, audio);
      }
    }

    if (speaking.bitfield === 0 && member.voice.channel)
    {
      this.clear = false
      let stream = this.listenStreams.get(member.id)
      if (stream) {
        this.listenStreams.delete(member.id)
        stream.end(err => {
          if (err) console.error(err)
        })

        let basename = path.basename(stream.path, '.opus_string')
        let text = 'default'

        this.processRawToWav(
            path.join('./recordings', basename),
            path.join('./recordings', member.id + '.wav'),
            (function (data) {
              if (data != null) {
                this.handleSpeech(member, data.text)
              }
            }).bind(this)
        )

      }
    }
  }

  handleSpeech (member, speech) {
    if (speech != undefined) {
      console.log("Wit.AI Heard: " + speech);
      let voice_data = speech.toLowerCase().split(' ')
      let command = voice_data[0]

      if (command == "thanks" && voice_data[1] == "friday")
        general.sayYoureWelcome(this.client, this.listenConnection);

      if (command == "friday") {
        if (voice_data[1] == "add")
        {
          phasmo.addEvidence(voice_data, this.client, this.listenConnection);
        }
        else if (voice_data[1] == "identify" || voice_data[1] == "identified" && voice_data[2] == "ghost")
        {
          phasmo.whatGhost(this.client, this.listenConnection);
        }
        else if (voice_data[1] == "new" && voice_data[2] == "game")
        {
          phasmo.newGame(this.client, this.listenConnection);
          console.log("New game?");
        }
        else if (voice_data[1] == "remove" && voice_data[2] == "last" && voice_data[3] == "evidence")
        {
          phasmo.removeLastEvidence(this.client, this.listenConnection);
        }
        else if (voice_data[1] == "list" || voice_data[1] == "playlist" && voice_data[2] == "evidence" || voice_data[2] == "advance")
        {
          phasmo.listEvidence(this.client, this.listenConnection);
        }
        else if (voice_data[1] == "explain")
        {
          phasmo.listTraits(this.client, this.listenConnection, voice_data[2])
        }
        else if (voice_data[1] == "solitary" && voice_data[2] == "confinement" || voice_data[1] == "activate" && voice_data[2] == "horny" && voice_data[3] == "protocol")
        {
          general.sendAbeToHornyJail(this.client, this.listenConnection);
        }
        else if (voice_data[1] == "what" && voice_data[2] == "time" && voice_data[3] == "is" && voice_data[4] == "it")
        {
          general.whatTimeIsIt(this.client, this.listenConnection);
        }
      }
    }
    this.clearFiles(member.id);
  }

  processRawToWav(filepath, outputpath, cb) {
    let witai_key = config.WIT_AI.key
    const ffmpeg = require('fluent-ffmpeg')
    // console.log('processing raw data to wav data')
    fs.closeSync(fs.openSync(outputpath, 'w'));
    let command = ffmpeg(filepath)
        .addInputOptions([ '-f s32le', '-ar 48k', '-ac 1' ])
        .on('end', function() {
          // Stream the file to be sent to the wit.ai
          let stream = fs.createReadStream(outputpath);
          let parseSpeech =  new Promise((ressolve, reject) => {
            // call the wit.ai api with the created stream
            // console.log('Trying with: ' + witai_key)
            WitSpeech.extractSpeechIntent(
                witai_key, stream,
                'audio/wav',
                (err, res) => {
                  if (err) return reject(err);
                  ressolve(res);
                })
          })

          // witai call complete
          parseSpeech.then((data) => {
            cb(data)
          }).catch((err) => {
            console.log(err);
          })
        })
        .on('error', function(err) {
          console.log('An error occured in ffmpeg(filepath): ' + err.message);
        })
        .addOutput(outputpath)
        .run();
  }

  clearFiles(memberId)
  {
    if (this.clear === false) {
      let clearRecordings = () => {
        const fs = require('fs');
        const path = require('path');
        const directory = './recordings';
        fs.readdir(directory, (err, files) => {
          if (err) throw err;
          for (const file of files) {
            if (file.includes(memberId)) {
              fs.unlink(path.join(directory, file), err => {
                if (err) console.log('Error: ', err)
              })
            }
          }
        })
      }
      clearRecordings()
      this.clear = true
    }
  }

}